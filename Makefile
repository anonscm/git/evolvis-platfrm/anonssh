DEFS+=	-D_GNU_SOURCE
DEFS+=	-D_FILE_OFFSET_BITS=64

.ifdef DEBUG
DEFS+=	-DDEBUG
.endif

# Specify this if you want to log :pserver: connections
# and unauthorised access.
DEFS+=	-DUSE_SYSLOG

# path to the chroot, repos, etc.
.ifdef NO_CHROOT
DEFS+=	-DNO_CHROOT
.else
DEFS+=	-DCHROOT_PATH=\"/var/lib/gforge/chroot\"
.endif
.ifdef USE_SVN
DEFS+=	-DPATH_SVN=\"/usr/bin/svnserve\"
.endif
.ifdef USE_SFTP
DEFS+=	-DPATH_SFTP=\"/usr/lib/openssh/sftp-server\"
.endif
.ifdef USE_GIT
DEFS+=	-DPATH_GIT=\"/usr/bin/git\"
.endif
.ifndef NO_RSYNC
DEFS+=	-DACCESS_RSYNC
.endif

# Name of this programme. Set e.g. to "anoncvssh.mirbsd"
# if running more than one anoncvs user, and for read-
# write users, to "anoncvs-username" (thus you can place
# them in the common parent dir).
PROG?=	anonsvnsh${PROGSUFF}

CPPFLAGS+=	${DEFS} -DANONCVSSH_NAME=\"${PROG}\"
CDIAGFLAGS+=	-Wno-cast-qual
BINOWN=		root
BINGRP=		root
BINMODE=	4111
BINDIR=		/lib
#LDFLAGS+=	-static
SRCS=		anoncvssh.c
NOMAN=		yes

anoncvssh.o: Makefile

.ifdef SRCDIR
.PATH: ${SRCDIR}
.endif

.include <bsd.prog.mk>
