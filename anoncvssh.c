/* From MirOS: src/libexec/anoncvssh/anoncvssh.c,v 1.9 2008/06/07 20:25:43 tg Exp $ */

/*-
 * Copyright (c) 2010, 2011, 2012, 2013, 2014, 2017
 *	Thorsten Glaser <t.glaser@tarent.de>
 * Copyright (c) 2007
 *	Thorsten Glaser <t.glaser@aurisp.de>
 * Copyright (c) 2004, 2005, 2006, 2008
 *	Thorsten "mirabilos" Glaser <tg@mirbsd.org>
 *
 * Provided that these terms and disclaimer and all copyright notices
 * are retained or reproduced in an accompanying document, permission
 * is granted to deal in this work without restriction, including un-
 * limited rights to use, publicly perform, distribute, sell, modify,
 * merge, give away, or sublicence.
 *
 * This work is provided "AS IS" and WITHOUT WARRANTY of any kind, to
 * the utmost extent permitted by applicable law, neither express nor
 * implied; without malicious intent or gross negligence. In no event
 * may a licensor, author or contributor be held liable for indirect,
 * direct, other damage, loss, or other issues arising in any way out
 * of dealing in the work, even if advised of the possibility of such
 * damage or existence of a defect, except proven that it results out
 * of said person's immediate fault when using the work as intended.
 *-
 * user shell to be used for chrooted access (anonymous or personali-
 * sed, read-only or read-write) to cvs and possibly rsync.
 * This programme requires ANSI C.
 */

/*
 * Copyright (c) 2002 Todd C. Miller <Todd.Miller@courtesan.com>
 * Copyright (c) 1997 Bob Beck <beck@obtuse.com>
 * Copyright (c) 1996 Thorsten Lockert <tholo@sigmasoft.com>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <sys/types.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <sys/stat.h>
#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#if defined(__OpenBSD__) || defined(__NetBSD__) || defined(__FreeBSD__)
#include <paths.h>
#endif
#include <pwd.h>
#include <unistd.h>

/*
 * You may need to change this path to ensure that RCS, CVS and diff
 * can be found
 */
#ifndef _PATH_DEFPATH
#define _PATH_DEFPATH	"/bin:/usr/bin:/usr/sbin"
#endif

/*
 * This should not normally have to be changed
 */
#ifndef _PATH_BSHELL
#define _PATH_BSHELL	"/bin/sh"
#endif

/*
 * This is our own programme name
 */
#ifndef ANONCVSSH_NAME
#define ANONCVSSH_NAME	"anoncvssh"
#endif

/*
 * This is the rsync server to invoke
 */
#define FULL_RSYNC	"rsync --server "


/****************************************************************/

#ifdef USE_SYSLOG
#include <string.h>
#include <syslog.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#define LOG_FACILITY	LOG_DAEMON
#define LOG_PRIO	LOG_INFO
#define DO_LOG(x, ...)	syslog(LOG_NOTICE, x, ## __VA_ARGS__)
#define DO_LOG0(x)	syslog(LOG_NOTICE, x)
#else /* def USE_SYSLOG */
#define DO_LOG(x, ...)	/* nothing */
#define DO_LOG0(x)	/* nothing */
#endif /* ! def USE_SYSLOG */

char env_path[] = "PATH=" _PATH_DEFPATH;
char env_shell[] = "SHELL=" _PATH_BSHELL;
char env_cvsumask[] = "CVSUMASK=002";

char *env[] = {
	NULL /* placeholder for $HOME */,
	env_path,
	env_shell,
	env_cvsumask,
	NULL
};

const char *git_commands[] = {
	"git-receive-pack ",
	"git-upload-archive ",
	"git-upload-pack ",
	NULL
};

int
main(int argc, char *argv[])
{
	struct passwd *pw;
#ifndef NO_CHROOT
	char *chrootdir;
#endif
	char *s;
	char *homedir;
	char *uname;
	int i;

#ifdef USE_SYSLOG
	openlog(ANONCVSSH_NAME, LOG_PID | LOG_NDELAY, LOG_FACILITY);
#endif

	/* ensure stdin/out/err are always open, to avoid problems */
	i = open("/dev/null", O_RDWR);
	while (i >= 0 && i <= 2)
		i = dup(i);
	if (i == -1) {
		DO_LOG0("cannot open or dup /dev/null\n");
		fprintf(stderr, "internal error\n");
		exit(1);
	}
	close(i);

	/* read passwd entry of user */
	pw = getpwuid(getuid());
	if (pw == NULL) {
		fprintf(stderr, "no user for uid %d\n", getuid());
		exit(1);
	}
	if (pw->pw_dir == NULL) {
		fprintf(stderr, "no home directory\n");
		exit(1);
	}

#ifdef DEBUG
	/* dump calling information */
	DO_LOG("opening, argc=%d, dumping argv\n", argc);
	{
		int i = 0;
		while (i < argc) {
			DO_LOG(" argv[%d] = '%s'\n", i, argv[i]);
			++i;
		}
	}
	DO_LOG("running for user %d (%s)\n", getuid(), pw ? pw->pw_name : "<NULL>");
#endif

	/* get root */
	if (setuid(0)) {
		perror("setuid root");
		exit(1);
	}
	/* prepare user home path */
#ifndef NO_CHROOT
	if ((chrootdir = strdup(CHROOT_PATH)) == NULL) {
		perror("strdup");
		exit(1);
	}

	if (strncmp(pw->pw_dir, chrootdir, strlen(chrootdir))) {
                DO_LOG("user's home directory (%s) not inside chroot (%s)! UID is %d.\n", pw->pw_dir, chrootdir, getuid());
//		DO_LOG0("user's home directory not inside chroot!\n");
		exit(1);
	}

	s = pw->pw_dir + strlen(chrootdir);
	if (*s != '/') {
		DO_LOG0("No leading slash in user's home directory!\n");
		exit(1);
	}
#else
	s = pw->pw_dir;
#endif

	/* set up environment */
	if ((homedir = strdup(s)) == NULL) {
		perror("strdup");
		exit(1);
	}
	if ((uname = strdup(pw->pw_name ? : "(null)")) == NULL) {
		perror("strdup");
		exit(1);
	}

	if (asprintf(&env[0], "HOME=%s", homedir) < 0) {
		perror("asprintf");
		exit(1);
	}

	/* chroot (if desired) and cd to user HOME */
#ifndef NO_CHROOT
	if (chroot(chrootdir) == -1) {
		perror("chroot");
		exit(1);
	}
#endif
	if (chdir(homedir)) {
		perror(homedir);
		fprintf(stderr, "trying to chdir / instead\n");
		if (chdir("/")) {
			perror("chdir");
			exit(1);
		}
	}

#ifdef DEBUG
	/* log someone is there */
	DO_LOG("anonsvnsh: authenticated user %u (%s)\n",
	    (unsigned)pw->pw_uid, uname);
	DO_LOG("chrooted to '%s' with home directory '%s'\n",
/*XXX this will not work with newer compilers! */
#ifndef NO_CHROOT
	    chrootdir,
#else
	    "/",
#endif
	    homedir);
#endif

	/* switch to the user */
	if (setuid(pw->pw_uid)) {
		perror("setuid user");
		exit(1);
	}
#ifndef NO_CHROOT
	free(chrootdir);
#endif
	free(homedir);

	/*
	 * set umask 002 to keep group-write permissions on SCM repos
	 */
	/* note: leading-zero numbers are octal (base-8) numbers! */
	umask(002);

	/*
	 * programme now "safe"
	 */

#ifdef PATH_SFTP
	/* SFTP support */
	if (argc == 3 && !strcmp(argv[1], "-c") &&
	    !strcmp(argv[2], PATH_SFTP)) {
#ifdef DEBUG
		DO_LOG("spawning '%s'\n", PATH_SFTP);
#endif
		execle(PATH_SFTP, PATH_SFTP, NULL, env);
		perror("execle: sftp-server");
		DO_LOG0("chaining to sftp-server failed!");
		fprintf(stderr, "unable to exec SFTP server!\n");
		return (1);
	}
#endif /* PATH_SFTP */

	if (argc == 3 && !strcmp(argv[1], "-c") &&
	    !strncmp(FULL_RSYNC, argv[2], strlen(FULL_RSYNC))) {
#ifdef ACCESS_RSYNC
		int i = -1;
		char *newarg[256];
		char *p = argv[2];

 lp:
		newarg[++i] = strsep(&p, " ");
		if ((newarg[i] != NULL) && (i < 255))
			goto lp;
#ifdef DEBUG
		argc = i;
		DO_LOG("calling rsync; argc = %d\n", argc);
		for (i = 0; i < argc; i++)
			DO_LOG("newarg[%d] = \"%s\"\n", i, newarg[i]);
#endif
		execve("/usr/bin/rsync", newarg, (char **)env);
		perror("execve: rsync");
		DO_LOG0("chaining to rsync failed!");
		fprintf(stderr, "unable to exec RSYNC!\n");
		exit(1);
		/* NOTREACHED */
#else
		DO_LOG0("access to RSYNC prohibited!");
#endif
	}

#ifdef PATH_GIT
	/* git support */
	if (argc == 3 && !strcmp(argv[1], "-c") &&
	    !strncmp(argv[2], "git", 3)) {
		const char **ccp;
		char *d;

		/* make "git foo" == "git-foo" */
		if (argv[2][3] == ' ')
			argv[2][3] = '-';
		/* check for valid command */
		ccp = git_commands;
		while (*ccp)
			if (!strncmp(argv[2], *ccp, strlen(*ccp)))
				break;
			else
				++ccp;
		if (!*ccp) {
 git_weird:
			DO_LOG("weird git command: %s\n", argv[2]);
			fprintf(stderr, "bogus git command!\n");
			exit(1);
		}
		/* unquote argument according to git rules */
		d = s = (argv[2] += strlen(*ccp));
		if (*s != '\'')
			/* must be quoted */
			goto git_weird;
 git_unquote_inner:
		switch (*++s) {
		case '\0':
			/* unexpected EOS */
			goto git_weird;
		case '\'':
			/* end of quoted string */
			break;
		default:
			/* regular quoted character */
			*d++ = *s;
			goto git_unquote_inner;
		}
 git_unquote_outer:
		switch (*++s) {
		case '\0':
			/* expected EOS */
			break;
		case '\'':
			/* back to quoted string */
			goto git_unquote_inner;
		case '\\':
			/* escaped... what? */
			switch (*++s) {
			case '!':
				/* escaped exclamation mark is allowed */
				/* FALLTHROUGH */
			case '\'':
				/* apostrophe must be escaped */
				*d++ = *s;
				goto git_unquote_outer;
			}
			/* FALLTHROUGH */
		default:
			/* anything else is invalid */
			goto git_weird;
		}
		*d = '\0';
		/* argv[2] is now a single unquoted argument */

		if (argv[2][0] == '-') {
			/* CVE-2017-8386 */
			goto git_weird;
		}

		/* calculate the git command */
		if (asprintf(&s, "%s%s", PATH_GIT, *ccp + 3) <= 0) {
			DO_LOG("cannot asprintf %s + %s\n", PATH_GIT, *ccp + 3);
			fprintf(stderr, "asprintf failed\n");
			exit(1);
		}
		/* drop trailing space */
		s[strlen(s) - 1] = '\0';

		/* call the git command */
#ifdef DEBUG
		DO_LOG("spawning %s(%s) '%s'\n", *ccp, s, argv[2]);
#endif
		execle(s, s, argv[2], NULL, env);

		perror("execle: git");
		DO_LOG0("chaining to git failed!");
		fprintf(stderr, "unable to exec git server!\n");
		exit(1);
	}
#endif /* PATH_GIT */

#ifdef PATH_SVN
	/* just execute svn, no matter what the client said */
#ifdef DEBUG
	DO_LOG("spawning '%s' -t --tunnel-user '%s'\n", PATH_SVN, uname);
#endif
	execle(PATH_SVN, "svnserve", "-t", "--tunnel-user", uname, NULL, env);

	perror("execle: svnserve");
	DO_LOG0("chaining to svn failed!");
	fprintf(stderr, "unable to exec Subversion server!\n");
#else
	fprintf(stderr, "no server configured!\n");
#endif
	exit(1);
}
